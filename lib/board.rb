require 'byebug'

class Board
  attr_reader :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    grid = Array.new(10) { Array.new(10) }
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def count
    ships = 0
    (0...grid.size).each do |row|
      (0...grid[0].size).each do |col|
        ships += 1 if grid[row][col] == :s
      end
    end

    ships
  end

  def empty?(pos = nil)
    if pos.nil?
      (0...grid.size).each do |row|
        (0...grid[0].size).each do |col|
          return false unless grid[row][col].nil?
        end
      end
    else
      row, col = pos
      return true if grid[row][col].nil?
    end
  end

  def full?
    # eventually change to return false if # of ships reached max
    (0...grid.size).each do |row|
      (0...grid[0].size).each do |col|
        return false if grid[row][col].nil?
      end
    end

    true
  end

  def place_random_ship
    raise "board is full" if full?

    row = rand(grid.size)
    col = rand(grid.size)
    @grid[row][col] = :s if empty?([row, col])
  end

  def populate_grid
    #place ships randomly until max ships reached
  end

  def in_range?(pos)

  end

  def display

  end

  def won?
    return false unless count.zero?
    true
  end
end

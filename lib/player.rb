class HumanPlayer
  def initialize (name)
    @name = name
  end

  def get_play
    # get play in a string then turn into an Array
    # play = [x, y]
    puts "Enter position to strike (e.g. 0, 0):"
    input = gets.chomp
    pos = input.delete("^0-9")

    [pos[0], pos[1]]
  end
end

class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(position)
    row, col = position
    board.grid[row][col] = :x
  end

  def count
    board.count
  end

  def play_turn
    pos = player.get_play
    attack(pos)
  end

  def game_over?
    puts "No more ships. Game Over!" if board.won?
  end
end
